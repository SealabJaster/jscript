﻿/++
 + The module containing everything regarding the creation of tokens.
 + ++/
module jscript.lexer;

private
{
    import std.range, std.array, std.algorithm, std.exception;
    import std.format : format;
}

/// Contains all the different types of tokens.
enum TokenType
{
    // Keywords
    KEY_DECLARE,                // Declare

    // Operators
    OP_LBRACKET,                // (
    OP_RBRACKET,                // )

    // Unique types
    UNIQ_EOF,                    // End of file
    UNIQ_STRING,                 // String
    UNIQ_NUMBER,                 // Number
    UNIQ_IDENTIFIER,             // An Identifier(Looks like a keyword, but doesn't have any meaning)
    UNIQ_UNKNOWN,                // An unknown(therefor invalid) type.
}

/// Contains debug information about a token
struct TokenDebugInfo
{
    /// The file the token was made in.
    string file;

    /// The line the token was made.
    size_t line;
}

/// Contains data about a token
struct Token
{
    /// What kind of token this is.
    TokenType       type;

    /// The text that makes up the token.
    string          text;

    /// Debug information for the token.
    TokenDebugInfo  info;
}

/// Lexes source code into Tokens.
class Lexer
{
    /// Private variables (I like to keep private functions and variables seperated, if the private functions are massive)
    private
    {
    }

    /// Private functions
    private
    {
    }
}

/// The exception that is thrown by the Lexer.
class LexerException : Exception
{
    public
    {
        /**
         * Creates a new instance of Exception. The next parameter is used
         * internally and should always be $(D null) when passed by user code.
         * This constructor does not automatically throw the newly-created
         * Exception; the $(D throw) statement should be used for that purpose.
         */
        @nogc @safe pure nothrow this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
        {
            super(msg, file, line, next);
        }
    }
}

private
{
    // This function exists becauase I'm *super* lazy, and 'case "whatevs": return TokenType.SASS_OVERLOAD;' is too much work to write.
    /++
     + Generates cases for a switch statement.
     + General format for the template parameters is: ("someValue", enumThing)
     + TODO: Finish this god-awful documentation.
     + ++/
    string generateCases(T...)()
    {
        static assert((T.length > 0) && (T.length % 2) == 0, "The amount of arguments given must be even.");
        string code = "";
        
        string value = "";
        foreach(i, thing; T)
        {
            if((i % 2) == 1 && (i != 0))
            {
                code ~= format("case %s: return %s;", value, thing.stringof);
                continue;
            }
            else
            {
                value = thing.stringof;
            }
        }
        
        return code;
    }
    unittest
    {
        auto test()
        {
            switch("This is just to make sure it compiles")
            {
                mixin(generateCases!(
                        "This", TokenType.UNIQ_EOF,
                        "Help Me", TokenType.UNIQ_STRING,
                        "No", TokenType.UNIQ_NUMBER
                        ));
                
                default:
                    break;
            }
            
            switch(';')
            {
                mixin(generateCases!(
                        '.', TokenType.UNIQ_IDENTIFIER,
                        '2', TokenType.UNIQ_EOF
                        ));
                
                default:
                    break;
            }
            return TokenType.UNIQ_IDENTIFIER;
        }
    }

    /++
     + Finds out and returns the type that $(B text) represents.
     + 
     + Example:
     + ---
     +  assert(toTokenType("Declare")    == TokenType.KEY_DECLARE);
     +  assert(toTokenType("I'm a boob") == TokenType.UNIQ_UNKNOWN);
     + ---
     + 
     + Parameters:
     +  text = The text to convert.
     + 
     + Returns:
     +  The $(B TokenType) that $(B text) represents.
     +  TokenType.UNIQ_UNKNOWN is returned if $(B text) does not represent a TokenType.
     + ++/
    TokenType toTokenType(C)(C text)
    if(is(C == string) || is(C == char))
    {
        static if(is(C == string))
        {
            switch(text)
            {
                mixin(generateCases!(
                        "Declare",      TokenType.KEY_DECLARE
                        ));
                
                default:
                    return TokenType.UNIQ_UNKNOWN;
            }
        }
        else
        {
            switch(text)
            {
                mixin(generateCases!(
                        '(', TokenType.OP_LBRACKET,
                        ')', TokenType.OP_RBRACKET
                        ));
                
                default:
                    return TokenType.UNIQ_UNKNOWN;
            }
        }
    }
    unittest
    {
        assert(toTokenType("Declare")    == TokenType.KEY_DECLARE);
        assert(toTokenType("I'm a boob") == TokenType.UNIQ_UNKNOWN);
        assert(toTokenType(')')          == TokenType.OP_LBRACKET);
        assert(toTokenType('2')          == TokenType.UNIQ_UNKNOWN);
    }
}